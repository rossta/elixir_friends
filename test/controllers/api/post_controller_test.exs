defmodule ElixirFriends.API.PostControllerTest do
  use ElixirFriends.ConnCase
  alias ElixirFriends.Post

  setup do
    conn = build_conn() |> put_req_header("accept", "application/json")
    {:ok, conn: conn}
  end

  test "lists all posts", %{conn: conn} do
    post = %Post{
      image_url: "http://elixirfriends.com",
      content: "this is some content",
      username: "rossta",
      source_url: "http://elixirfriends.com"
    }
    inserted_post = post |> ElixirFriends.Repo.insert!
    conn = get conn, "/api/posts"

    expected_response = %{
      total_pages: 1,
      total_entries: 1,
      page_size: 20,
      page_number: 1,
      entries: [inserted_post]
    } |> Poison.encode!

    assert json_response(conn, 200) == expected_response
  end
end
