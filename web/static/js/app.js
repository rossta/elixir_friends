// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "deps/phoenix_html/web/static/js/phoenix_html"

import React from "react"
import ReactDOM from "react-dom"
import socket from "./socket"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"

const PostList = React.createClass({
  getInitialState() {
    return {
      posts: []
    }
  },

  componentDidMount() {
    $.get("/api/posts", result => {
      result = JSON.parse(result);
      this.setState({posts: result.entries});
    });

    this.subscribeToNewPosts();
  },

  subscribeToNewPosts() {
    socket.connect();
    let channel = socket.channel("posts:new", {});
    channel.join().receive("ok", chan => {
      console.log("joined", this);
    });
    channel.on("new:post", post => {
      this.injectNewPost(post)
    });
  },

  injectNewPost(post) {
    this.setState({
      posts: [post].concat(this.state.posts)
    });
  },

  render() {
    return(
      <div>
        <h2>Listing posts (React rendered)</h2>

        <p>
          ok at all these people!  They came together because of Elixir!
        </p>
        <div className="ui grid stackable">
          {this.state.posts.map(function(post, i) {
            return <Post
              key={i}
              imageUrl={post.image_url}
              username={post.username}
              insertedAt={post.inserted_at}
              content={post.content} />
          })}
        </div>
      </div>
    );
  }
});

const Post = React.createClass({
  render() {
    return (
      <div className="four wide column">
        <div className="ui card">
          <div className="image">
            <img src={this.props.imageUrl} />
          </div>
          <div className="content">
            <div className="header">
            {this.props.username}
            </div>
            <div className="meta">
              <span className="date">{this.props.insertedAt}</span>
            </div>
            <div className="description">
              {this.props.content}
            </div>
          </div>
        </div>
      </div>
    )
  }
});

window.onload = () => {
  const element = document.getElementById("app");
  return ReactDOM.render(<PostList />, element);
}
