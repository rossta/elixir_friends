defmodule ElixirFriends.PostView do
  use ElixirFriends.Web, :view

  @total_links 10

  def pagination_links(page_number, total_pages) do
    links = page_numbers(page_number, total_pages)
      |> Enum.map(&page_link/1)

    content_tag(:div, links, class: "ui buttons clearfix")
  end

  defp page_numbers(page_number, total_pages) do
    page_numbers = (page_number..total_pages) |> Enum.to_list

    page_numbers = if page_number !== 1 do
      [{"<<", page_number-1}, 1] ++ page_numbers
    else
      page_numbers
    end

    page_numbers = Enum.take(page_numbers, @total_links)

    page_numbers = if page_number == total_pages do
      page_numbers
    else
      page_numbers ++ [{">>", page_number+1}]
    end

    page_numbers
  end

  defp page_link({text, page_number}) do
    content_tag(:div, link("#{text}", to: "?page=#{page_number}", class: "ui button"))
  end
  defp page_link(page_number) do
    page_link({page_number, page_number})
  end
end
