defmodule ElixirFriends.PostsChannel do
  use Phoenix.Channel

  def join("posts:new", _auth_msg, socket) do
    IO.puts "New connection to the PostsChannel"
    {:ok, socket}
  end
end
